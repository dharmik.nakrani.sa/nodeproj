FROM node:latest
WORKDIR /node-app
COPY . .
RUN npm install
EXPOSE 3000

CMD ["node","index.js"]
